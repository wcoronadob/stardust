import unittest
from maestro import get_price


class TestMaestro(unittest.TestCase):

    def test_get_price(self):
        self.assertEqual(get_price(), 'S/ 572.90')
        self.assertEqual(get_price(), 'S/ 572.0')

#
# if __name__ == '__main__':
#     unittest.main()
