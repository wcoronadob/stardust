import requests
from bs4 import BeautifulSoup


def get_price_sodimac():
    url = 'https://www.sodimac.com.pe/sodimac-pe/product/2389746/Cemento-Mochica-42.5-Kg/2389746'
    raw_input = requests.get(url)

    soup = BeautifulSoup(raw_input.content, 'html.parser')

    cool_stuff = soup.find("div", {"class": "pdp-sticky-header-price"})

    return str(cool_stuff)


print(get_price_sodimac())