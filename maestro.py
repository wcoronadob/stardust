import json
import requests
from bs4 import BeautifulSoup


def get_price():
    url = 'https://www.maestro.com.pe/productos/gasfiteria/tanque-de-agua-1'
    store = 46
    page = requests.get(url)

    if page.status_code == 200:
        soup = BeautifulSoup(page.content, 'html.parser')
        raw_data = soup.find("input", {'id': 'ptiendaxciudad'})['value']
        # product_name = soup.find("div", {'class': 'detail'})
        if raw_data:
            data = json.loads(raw_data)
            for item in data[str(store)]:
                # return str(product_name.text) + ' : ' + str(item)
                return item
        else:
            raise ValueError('The product does not found')

    else:
        raise ValueError('Something is worn with the page')


print('Este el precio de: ' + get_price())

